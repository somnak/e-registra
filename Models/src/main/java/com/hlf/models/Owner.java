package com.hlf.models;

import java.util.List;

public class Owner {
    private String Name;
    private String Age;
    private String Address;
    private List<Car> carList;

    public Owner() {
    }

    public Owner(String name, String age, String address, List<Car> carList) {
        Name = name;
        Age = age;
        Address = address;
        this.carList = carList;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public List<Car> getCarList() {
        return carList;
    }

    public void setCarList(List<Car> carList) {
        this.carList = carList;
    }

    @Override
    public String toString() {
        return "Owner{" +
                "Name='" + Name + '\'' +
                ", Age='" + Age + '\'' +
                ", Address='" + Address + '\'' +
                ", carList=" + carList +
                '}';
    }
}
