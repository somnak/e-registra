package com.hlf.models;

import java.util.List;

public class Car {
    private String SerialNumber;
    private String Maker;
    private String ModelName;
    private String Color;
    private List<Owner> ownerList;

    public Car() {
    }

    public Car(String serialNumber, String maker, String modelName, String color, List<Owner> ownerList) {
        SerialNumber = serialNumber;
        Maker = maker;
        ModelName = modelName;
        Color = color;
        this.ownerList = ownerList;
    }

    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        SerialNumber = serialNumber;
    }

    public String getMaker() {
        return Maker;
    }

    public void setMaker(String maker) {
        Maker = maker;
    }

    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String modelName) {
        ModelName = modelName;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public List<Owner> getOwnerList() {
        return ownerList;
    }

    public void setOwnerList(List<Owner> ownerList) {
        this.ownerList = ownerList;
    }

    @Override
    public String toString() {
        return "Car{" +
                "SerialNumber='" + SerialNumber + '\'' +
                ", Maker='" + Maker + '\'' +
                ", ModelName='" + ModelName + '\'' +
                ", Color='" + Color + '\'' +
                ", ownerList=" + ownerList +
                '}';
    }
}
