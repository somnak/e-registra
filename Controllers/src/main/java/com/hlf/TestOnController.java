package com.hlf;

import com.hlf.controllers.Controllers;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class TestOnController extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(TestOnController.class);
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext ap=SpringApplication.run(TestOnController.class, args);
        Controllers cs =ap.getBean(Controllers.class);
        cs.createCar();
    }
}
