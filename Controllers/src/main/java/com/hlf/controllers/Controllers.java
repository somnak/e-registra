package com.hlf.controllers;

import com.hlf.models.Car;
import com.hlf.models.Owner;
import com.hlf.services.InvokeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class Controllers {
    private InvokeServices invokeServices;
    @Autowired
    public Controllers(InvokeServices invokeServices) {
        this.invokeServices = invokeServices;
    }

    public void createCar() throws Exception {
        Owner owner=new Owner("SomNak","23","PP",null);
        List<Owner> ownerList=new ArrayList<>();
        ownerList.add(owner);
        Car car=new Car("1P32","Toyota","Scuba","Black",ownerList);
        Boolean createSuccess=invokeServices.createCar(car);
        if(createSuccess){
            System.out.println("Transaction Complete!");
        }else{
            System.out.println("Transaction fail!");
        }
    }
}
