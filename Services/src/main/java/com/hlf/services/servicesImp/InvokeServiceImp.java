package com.hlf.services.servicesImp;

import com.hlf.models.Car;
import com.hlf.models.Owner;
import com.hlf.repositories.InvokeRepository;
import com.hlf.services.InvokeServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvokeServiceImp implements InvokeServices {
    private InvokeRepository repo;
    @Autowired
    public InvokeServiceImp(InvokeRepository repo) {
        this.repo = repo;
    }

    @Override
    public Boolean changeOwner(String SerialNumber, Owner owner) {
        return null;
    }

    @Override
    public Boolean createCar(Car car) throws Exception {
        return repo.createCar(car);
    }

    @Override
    public Boolean deleteCar(String SerialNumber) {
        return null;
    }
}
