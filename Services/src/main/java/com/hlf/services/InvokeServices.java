package com.hlf.services;

import com.hlf.models.Car;
import com.hlf.models.Owner;

public interface InvokeServices {
    Boolean changeOwner(String SerialNumber,Owner owner);
    Boolean createCar(Car car) throws Exception;
    Boolean deleteCar(String SerialNumber);
}
