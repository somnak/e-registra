package com.hlf.repositories;

import com.hlf.models.Car;
import com.hlf.models.UserContext;
import org.hyperledger.fabric.sdk.*;
import org.hyperledger.fabric.sdk.security.CryptoSuite;
import org.hyperledger.fabric_ca.sdk.HFCAClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Repository
public class InvokeRepository {
    public Boolean createCar(Car car) throws Exception {
        CryptoSuite cryptoSuite = CryptoSuite.Factory.getCryptoSuite();
        HFCAClient caClient = HFCAClient.createNewInstance("http://127.0.0.1:7054", null);
        caClient.setCryptoSuite(cryptoSuite);
        Enrollment adminEnrollment = caClient.enroll("admin", "admin");
        UserContext admin = new UserContext();
        admin.setName("admin");
        admin.setAccount("admin");
        admin.setMspId("CooconMSP");
        admin.setEnrollment(adminEnrollment);
        HFClient hfClient = HFClient.createNewInstance();
        hfClient.setCryptoSuite(cryptoSuite);
        hfClient.setUserContext(admin);
        Channel channel = hfClient.newChannel("mychannel");
        Peer peer = hfClient.newPeer("peer0.coocon.kshrd.com.kh", "grpc://127.0.0.1:7051");
        channel.addPeer(peer);
        Orderer orderer = hfClient.newOrderer("orderer.kshrd.com.kh", "grpc://127.0.0.1:7050");
        channel.addOrderer(orderer);
        channel.initialize();
        TransactionProposalRequest transactionProposalRequest = hfClient.newTransactionProposalRequest();
        ChaincodeID ccID = ChaincodeID.newBuilder()
                .setName("mycar1")
                .setVersion("2.2")
                .build();

        transactionProposalRequest.setChaincodeID(ccID);

        transactionProposalRequest.setFcn("createCar");
        transactionProposalRequest.setArgs(car.getSerialNumber(),car.getMaker(),car.getModelName(),car.getColor(),car.getOwnerList().get(0).getName(),
                car.getOwnerList().get(0).getAge(),car.getOwnerList().get(0).getAddress());
        Collection<ProposalResponse> responses = channel.sendTransactionProposal(transactionProposalRequest, channel.getPeers());
        Collection<ProposalResponse> invalid = responses.stream().filter(r -> r.isInvalid()).collect(Collectors.toList());

        if (!invalid.isEmpty()) {
            invalid.forEach(response -> {
                System.out.println(response.getMessage());
            });
//            throw new RuntimeException("invalid response(s) found");
            return false;
        }
        BlockEvent.TransactionEvent event = channel.sendTransaction(responses).get(100, TimeUnit.SECONDS);

        if (event.isValid()) {
            return true;
        } else {
            return false;
        }
    }
}
